#ifndef SESSIONSMODEL_H
#define SESSIONSMODEL_H

#include <map>
#include <memory>
#include <mutex>

#include "ServerSessionIface.h"
#include "SesssionPublic.h"

class SessionsModel
{
public:
    SessionsModel();

    void addSession(std::shared_ptr<ServerSession> &session);
    std::shared_ptr<SessionPublic> getSession(int index);
    void removeSession(std::shared_ptr<ServerSession> session);
    ulong countSession();
    std::vector<long long> ids();
    std::shared_ptr<ServerSession> getSessionById(long long id);

private:
    std::map<long long, std::shared_ptr<ServerSession>>::iterator findSession(std::shared_ptr<ServerSession> session);
    std::map<long long, std::shared_ptr<ServerSession>> m_sessions;
    std::mutex _deleteSessionMutex;
    long long counter;
};

#endif // SESSIONSMODEL_H
