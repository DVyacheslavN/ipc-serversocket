/*
** Copyright (c) 2019 Domnin V.N.
** All rights reserved
**
**
*/

#ifndef SERVER_H
#define SERVER_H

#include <cstdlib>
#include <boost/asio.hpp>

#include "ServerIface.h"
#include "Package.h"

using boost::asio::ip::tcp;
using boost::asio::io_service;

class ServerSession;
class SessionsModel;

namespace Parser {
class TcpDataParser;
struct TcpPackage;
}

class ServerPrivate : public ServerIface
{
public:
    ServerPrivate(std::string addres, ushort port, int threadAmount, long sessionBuffSize);
    virtual std::string ip() override;
    virtual std::vector<long long> sessionIds() override;
    virtual SessionPublic getSessionById(long long id) override;

    virtual void bindSessionStatusChanged(std::function<void (int, ServerSessionIface::Status)> &handler) override;
    virtual void bindServerStatusChanged(std::function<void (Status)> &handler) override;
private:


    virtual ~ServerPrivate() override;

    void run();

    void start_accept();
    void handle_accept(const boost::system::error_code & err);

    io_service m_service;
    tcp::endpoint m_endpoint;
    tcp::acceptor m_acceptor;
    std::shared_ptr<tcp::socket> m_socket;
    SessionsModel * const m_sessionModel;
    std::string m_ipAddress;
    std::function<void (int, ServerSessionIface::Status)> m_sessionStatusChanged;
    long m_sessionBufferSize;
};

#endif // SERVER_H
