/*
** Copyright (c) 2019 Domnin V.N.
** All rights reserved
**
**
*/

#ifndef SESSION_H
#define SESSION_H

#include "ServerSessionIface.h"

#include <memory>
#include <functional>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;
using boost::asio::io_service;

class SessionIface;

class ServerSession : public ServerSessionIface, public std::enable_shared_from_this<ServerSession>
{
public:
    ServerSession(const std::shared_ptr<tcp::socket> &socket, io_service *service, long bufferSize);
    virtual ~ServerSession() override;

    std::string ip() override;
    Status sessionStatus() override;
    bool sendPackage(const Package &package) override;

    void bindPackageRecived(std::function<void (const Package &)> handler) override;
    void bindSessionStatusChanged(std::function<void (Status)> handler) override;
    void bindErrorRecived(std::function<void (std::string)> handler) override;

    void bindSessionRemoved(std::function<void (std::shared_ptr<ServerSession>)> handler);

private:
    void handleSessionStatusChanged(const ServerSessionIface::Status &state);
    void handleRemoveSession();

    void readError(std::string message);
    void writeError(std::string messag);

    void handlePing();

    SessionIface * const m_session;

    std::function<void (const Package &)> m_handlerPackageRecived;
    std::function<void (std::shared_ptr<ServerSession>)> m_handlerSessionRemoved;
    std::function<void (ServerSessionIface::Status)> m_handlerSessionStatusChanged;
    std::function<void (std::string)> m_handlerErrorRecived;

    boost::asio::deadline_timer *const m_timer;
    ServerSessionIface::Status m_sessionStatus;
    io_service *m_service;
};

#endif // SESSION_H
