cmake_minimum_required(VERSION 2.8)

project(TcpServer)

function(includeFunction)
    include_directories("$ENV{INCULDE_DIR}/include")
    find_library(BaseIDC_LIBRARY BaseIDC BaseIDClib "$ENV{LIB_DIR}/bin/")
    link_directories("$ENV{LIB_DIR}/bin/")
    target_link_libraries(TcpServer ${BaseIDC_LIBRARY})
endfunction()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -Wall -fPIC")
# -fvisibility=hidden

file(GLOB HEADER_LIB "*.h")
file(GLOB SOURCE_LIB "src/*.cpp")
file(GLOB INCLUDE "${CMAKE_CURRENT_SOURCE_DIR}/inc/*.h")

file(GLOB INCLUDE_EXT "$ENV{INCULDE_DIR}/include/*.h")

add_library(TcpServer SHARED ${SOURCE_LIB} ${HEADER_LIB} ${INCLUDE})
set_target_properties(TcpServer PROPERTIES LINKER_LANGUAGE CXX)
target_include_directories(TcpServer PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
target_include_directories(TcpServer PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/inc")

includeFunction()

IF(CMAKE_BUILD_TYPE MATCHES Debug)
    message("Debug build.")
    target_compile_definitions(TcpServer PRIVATE -DTCP_SERVICE_DEBUG)
    install(TARGETS TcpServer LIBRARY DESTINATION $ENV{LIB_DIR}/bin)
    install(FILES ${INCLUDE} DESTINATION $ENV{INCULDE_DIR}/include)
ELSEIF(CMAKE_BUILD_TYPE MATCHES Release)
    message("Release build.")
    install(TARGETS TcpServer LIBRARY DESTINATION $ENV{LIB_DIR}/bin)
    install(FILES ${INCLUDE} DESTINATION $ENV{INCULDE_DIR}/include)
ELSE()
    message("Some other build type.")
    install(TARGETS TcpServer LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
    install(FILES ${INCLUDE} DESTINATION ${CMAKE_INSTALL_PREFIX}/include)
ENDIF()

add_library(libboost_system SHARED IMPORTED)
set_property(TARGET libboost_system PROPERTY IMPORTED_LOCATION /usr/lib/x86_64-linux-gnu/libboost_system.so)

add_library(boost_serialization SHARED IMPORTED)
set_property(TARGET boost_serialization PROPERTY IMPORTED_LOCATION /usr/lib/x86_64-linux-gnu/libboost_serialization.so)

target_link_libraries(TcpServer libboost_system)
target_link_libraries(TcpServer boost_serialization)

add_custom_target(ExternalSources
                  ALL SOURCES ${INCLUDE_EXT})
