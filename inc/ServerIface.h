#ifndef SERVERIFACE_H
#define SERVERIFACE_H

#include <SesssionPublic.h>

#include <functional>
#include <string>

class ServerIface
{
public:
    enum Status
    {
        Stopped = 0,
        Running
    } typedef Status;

    virtual std::string ip() = 0;
    virtual std::vector<long long> sessionIds() = 0;
    virtual SessionPublic getSessionById(long long id) = 0;

    virtual void bindSessionStatusChanged(std::function<void (int, ServerSessionIface::Status)> &handler) = 0;
    virtual void bindServerStatusChanged(std::function<void (Status)> &handler) = 0;
    static ServerIface* createServer(std::string ipAddress, ushort port, int threadAmount = 5, long sessionBuffSize = 1024);

    virtual ~ServerIface() {}

private:
};

#endif // SERVERIFACE_H
