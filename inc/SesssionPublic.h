#ifndef SESSIONPUBLIC_H
#define SESSIONPUBLIC_H

#include "ServerSessionIface.h"
class ServerSession;

class SessionPublic : public ServerSessionIface
{
public:
    SessionPublic(std::weak_ptr<ServerSession> session);

    std::string ip() override;
    Status sessionStatus() override;
    bool sendPackage(const Package &package) override;

    void bindPackageRecived(std::function<void (const Package &)> callBack_getPackage) override;
    void bindSessionStatusChanged(std::function<void (Status)> handlerSessionStatusChanged) override;
    void bindErrorRecived(std::function<void (std::string)> handler) override;

    ~SessionPublic() override;

    bool isValid();

private:
    std::weak_ptr<ServerSessionIface> m_sessionImpl;
    std::shared_ptr<ServerSessionIface> getSharedPtr();
};

#endif // SESSIONPUBLIC_H
