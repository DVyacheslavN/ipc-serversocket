/*
** Copyright (c) 2019 Domnin V.N.
** All rights reserved
**
**
*/

#include "Session.h"

#include <functional>
#include <boost/timer.hpp>

#include <SessionIface.h>

namespace
{
const std::vector<char> ClientPing = {'C','l','i','e','n','t','P','i','n','g'};
const std::vector<char> ServerReply = {'S','e','r','v','e','r','R','e','p','l','y'};
}


#ifdef TCP_SERVICE_DEBUG
#include <debug.h>
#define debug() mDebug()
#define warn() mWarning()
#define inform() mMessage()
#else
struct Stub {
    template<class T>
    Stub& operator<<(const T&){
        return *this;
    }
    template<class T, class X>
    Stub& write(T, X) {
        return *this;
    }
};
#define debug() Stub()
#define warn() Stub()
#define inform() Stub()
#endif


ServerSession::ServerSession(const std::shared_ptr<tcp::socket> &socket, io_service *service, long bufferSize)
    : m_session(SessionIface::CreateSession(service, socket, bufferSize))
    , m_handlerPackageRecived(nullptr)
    , m_handlerSessionRemoved(nullptr)
    , m_handlerSessionStatusChanged(nullptr)
    , m_timer(new std::remove_pointer_t<decltype (m_timer)>(*service))
    , m_sessionStatus(ServerSessionIface::Status::Disconnected)
    , m_service(service)
{
    m_session->bindReadErrorRecived(std::bind(&ServerSession::readError, this, std::placeholders::_1));
    m_session->bindWriteErrorRecived(std::bind(&ServerSession::writeError, this, std::placeholders::_1));
    m_session->bindPackageRecived([=](const Package & pack)
    {
        if (pack.data().size() == ::ClientPing.size())
        {
            if(std::strncmp(pack.data().data(), ::ClientPing.data(), ::ClientPing.size()) == 0)
            {
                handlePing();
                return;
            }
        }
        if (m_handlerPackageRecived != nullptr)
        {
            m_handlerPackageRecived(pack);
        }
    });

    debug() << "Start server sesion";
    m_session->doRead();
}

ServerSession::~ServerSession()
{
    debug() << "Delete ptr:" << this;
}

std::string ServerSession::ip()
{
    return m_session->ip();
}

bool ServerSession::sendPackage(const Package &package)
{
    m_session->sendPackage(package);
}

void ServerSession::bindPackageRecived(std::function<void (const Package &)> handler)
{
    m_handlerPackageRecived = handler;
}

void ServerSession::bindSessionStatusChanged(std::function <void (ServerSessionIface::Status)> handler)
{
    m_handlerSessionStatusChanged = handler;
}

void ServerSession::bindErrorRecived(std::function<void (std::string)> handler)
{
    m_handlerErrorRecived = handler;
}

ServerSessionIface::Status ServerSession::sessionStatus()
{
    return m_sessionStatus;
}

void ServerSession::bindSessionRemoved(std::function<void (std::shared_ptr<ServerSession>)> handler)
{
    m_handlerSessionRemoved = handler;
}

void ServerSession::handleSessionStatusChanged(const ServerSessionIface::Status &state)
{
    if (m_sessionStatus != state)
    {
        m_sessionStatus = state;
        if (m_handlerSessionStatusChanged == nullptr)
        {
            debug() << "m_handlerSessionStatusChanged is null";
            return;
        }
        m_handlerSessionStatusChanged(state);
    }
}

void ServerSession::handleRemoveSession()
{
    m_session->getSocket().close();
    handleSessionStatusChanged(ServerSessionIface::Status::Disconnected);
    if (m_handlerSessionRemoved != nullptr)
    {
        m_handlerSessionRemoved(shared_from_this());
    }
}

void ServerSession::readError(std::string message)
{
    if (m_handlerErrorRecived != nullptr)
    {
        m_handlerErrorRecived(message);
    }
    handleRemoveSession();
}

void ServerSession::writeError(std::string messag)
{
    if (m_handlerErrorRecived != nullptr)
    {
        m_handlerErrorRecived(messag);
    }
    handleRemoveSession();
}

void ServerSession::handlePing()
{
    handleSessionStatusChanged(m_session->getSocket().is_open()
                               ? ServerSessionIface::Connected
                               : ServerSessionIface::Disconnected);
    debug() << "";
    static Package pingPack(::ServerReply);
    m_session->sendPackage(pingPack);
}
