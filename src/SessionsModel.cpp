#include "SessionsModel.h"
#include "SesssionPublic.h"

#ifdef TCP_SERVICE_DEBUG
#include <debug.h>
#define debug() mDebug()
#define warn() mWarning()
#else
struct Stub {
    template<class T>
    Stub& operator<<(const T&){
        return *this;
    }
    template<class T, class X>
    Stub& write(T, X) {
        return *this;
    }
};
#define debug() Stub()
#define warn() Stub()
#endif

SessionsModel::SessionsModel()
    : counter(0)
{

}

void SessionsModel::addSession(std::shared_ptr<ServerSession> &session)
{
    counter++;
    warn() << "session was added:" << session;
    if (findSession(session) != m_sessions.end())
    {
        warn() << "The session already exists: session ptr:" << session;
        return;
    }
    m_sessions[counter] = session;
}

std::shared_ptr<SessionPublic> SessionsModel::getSession(int index)
{
    if (static_cast<uint>(index) >= m_sessions.size() || index < 0)
    {
        debug() << "Index out of range, index:" << index;
        return std::shared_ptr<SessionPublic>(nullptr);
    }

    return std::make_shared<SessionPublic>(std::weak_ptr<ServerSession>(m_sessions[static_cast<uint>(index)]));
}

void SessionsModel::removeSession(std::shared_ptr<ServerSession> session)
{
    std::lock_guard<decltype(_deleteSessionMutex)> look(_deleteSessionMutex);

    auto iterator = findSession(session);
    if (iterator == m_sessions.end())
    {
        warn() << "Session wasn't found, session ptr:" << session.get()
               << "count:" << countSession();
    }
    else
    {
        debug() << "delete Session ptr:" << session;
        m_sessions.erase(iterator);
    }
}

ulong SessionsModel::countSession()
{
    return m_sessions.size();
}

std::vector<long long> SessionsModel::ids()
{
    std::vector<long long> _ids;
    if (countSession() != 0)
    {
        for(auto &session : m_sessions)
        {
            _ids.push_back(session.first);
        }
    }
    return _ids;
}

std::shared_ptr<ServerSession> SessionsModel::getSessionById(long long id)
{
    for(auto &session : m_sessions)
    {
        if (session.first == id)
            return session.second;
    }
    return std::shared_ptr<ServerSession>(nullptr);
}

std::map<long long, std::shared_ptr<ServerSession>>::iterator
SessionsModel::findSession(std::shared_ptr<ServerSession> session)
{
    for (auto iterator = m_sessions.begin(); iterator != m_sessions.end(); iterator++)
    {
        if (iterator->second == session)
        {
            return iterator;
        }
    }
    return m_sessions.end();
}
