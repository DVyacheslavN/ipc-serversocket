/*
** Copyright (c) 2019 Domnin V.N.
** All rights reserved
**
**
*/

#include "Server.h"

#include <thread>
#include <iostream>
#include <memory>
#include <thread>

using namespace std;

#include "SessionIface.h"
#include "SessionsModel.h"

#include "Session.h"

#ifdef TCP_SERVICE_DEBUG
#include <debug.h>
#define debug() mDebug()
#define warn() mWarning()
#define inform() mMessage()
#else
struct Stub {
    template<class T>
    Stub& operator<<(const T&){
        return *this;
    }
    template<class T, class X>
    Stub& write(T, X) {
        return *this;
    }
};
#define debug() Stub()
#define warn() Stub()
#define inform() Stub()
#endif


ServerIface* ServerIface::createServer(std::string ipAddress, ushort port, int threadAmount, long sessionBuffSize)
{
    return new ServerPrivate(ipAddress, port, threadAmount, sessionBuffSize);
}

ServerPrivate::ServerPrivate(std::string addres, ushort port, int threadAmount, long sessionBuffSize)
    : m_endpoint(boost::asio::ip::address::from_string(addres), port),
      m_acceptor(m_service, m_endpoint),
      m_socket(std::make_shared<tcp::socket>(m_service)),
      m_sessionModel(new SessionsModel()),
      m_ipAddress(addres)
    , m_sessionStatusChanged(nullptr)
    , m_sessionBufferSize(sessionBuffSize)
{
    debug() << "thread:" << this_thread::get_id();

    start_accept();

    for (int i = 0; i < threadAmount; i++)
    {
        std::thread *tr = new std::thread(std::bind(&ServerPrivate::run, this));
        tr->detach();
    }
}

ServerPrivate::~ServerPrivate()
{
    if (m_sessionModel != nullptr)
    {
        delete m_sessionModel;
    }
}

std::string ServerPrivate::ip()
{
    return  m_ipAddress;
}

std::vector<long long> ServerPrivate::sessionIds()
{
    return m_sessionModel->ids();
}

SessionPublic ServerPrivate::getSessionById(long long id)
{
    return SessionPublic(m_sessionModel->getSessionById(id));
}

void ServerPrivate::bindSessionStatusChanged(std::function<void (int, ServerSessionIface::Status)> &handler)
{
    m_sessionStatusChanged = handler;
}

void ServerPrivate::bindServerStatusChanged(std::function<void (ServerIface::Status)> &handler)
{

}

void ServerPrivate::run()
{
    m_service.run();
}

void ServerPrivate::start_accept()
{
    debug() << "StartAcept";
    try
    {
        m_acceptor.async_accept(*m_socket, std::bind(&ServerPrivate::handle_accept,
                                                    this, std::placeholders::_1));
    }
    catch (boost::system::system_error e)
    {
        warn() << e.what();
    }
}

void ServerPrivate::handle_accept(const boost::system::error_code &err)
{
    if (err)
    {
        warn() << err.message();
        return;
    }

    debug() << "Accept" << "thread:" << this_thread::get_id();

    auto session = std::make_shared<ServerSession>(m_socket, &m_service, m_sessionBufferSize);
    m_sessionModel->addSession(session);
    session->bindSessionRemoved([=](std::shared_ptr<ServerSession> data)
    {
        m_sessionModel->removeSession(data);
        start_accept();
    });
    start_accept();
}
