#include "SesssionPublic.h"
#include "Session.h"

#ifdef TCP_SERVICE_DEBUG
#include <debug.h>
#define debug() mDebug()
#define warn() mWarning()
#else
struct Stub {
    template<class T>
    Stub& operator<<(const T&){
        return *this;
    }
    template<class T, class X>
    Stub& write(T, X) {
        return *this;
    }
};
#define debug() Stub()
#define warn() Stub()
#endif


SessionPublic::SessionPublic(std::weak_ptr<ServerSession> session)
    : m_sessionImpl(session)
{
}

bool SessionPublic::isValid()
{
    return getSharedPtr() != nullptr;
}

std::string SessionPublic::ip()
{
    return getSharedPtr() != nullptr ? getSharedPtr()->ip() : "Unknown";
}

ServerSessionIface::Status SessionPublic::sessionStatus()
{
    if (auto sesson = getSharedPtr())
    {
        return sesson->sessionStatus();
    }
    else
    {
        debug() << "m_sessionImpl is null";
        return ServerSessionIface::Disconnected;
    }
}

bool SessionPublic::sendPackage(const Package &package)
{
    if (getSharedPtr() != nullptr)
    {
        getSharedPtr()->sendPackage(package);
        return true;
    }
    else
    {
        debug() << "m_sessionImpl is null";
        return false;
    }
}

void SessionPublic::bindPackageRecived(std::function<void (const Package &)> handler)
{
    if (getSharedPtr() != nullptr)
    {
        getSharedPtr()->bindPackageRecived(handler);
    }
    else
    {
        debug() << "m_sessionImpl is null";
    }
}

void SessionPublic::bindSessionStatusChanged(std::function <void (Status)> handler)
{
    if (getSharedPtr() == nullptr)
    {
         debug() << "m_handlerSessionStatusChanged is null";
         return;
    }
    getSharedPtr()->bindSessionStatusChanged(handler);
}

void SessionPublic::bindErrorRecived(std::function<void (std::string)> handler)
{
    getSharedPtr()->bindErrorRecived(handler);
}

SessionPublic::~SessionPublic()
{
    //delete m_sessionImpl;
}

std::shared_ptr<ServerSessionIface> SessionPublic::getSharedPtr()
{
    return m_sessionImpl.lock();
}
